package uk.co.brightfuture.examplespringwebapp.web.selenium.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import uk.co.brightfuture.examplespringwebapp.web.lib.selenium.Page;
import uk.co.brightfuture.examplespringwebapp.web.lib.selenium.PagePath;
import uk.co.brightfuture.examplespringwebapp.web.lib.selenium.PageTitle;

public class IndexPage extends Page {

	public IndexPage (WebDriver driver) {
		super(driver, PageTitle.BASEURL, PagePath.BASEURL);

		// COMPULSORY
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	

}
