package uk.co.brightfuture.examplespringwebapp.web.lib.selenium;

/**
 * Used with doc and doc link objects for doc type.
 * 
 * @author JWatkins
 *
 */
public class PagePath {

	public static final String BASEURL = "http://localhost:8080/ExampleSpringWebApp/";
	public static final String HOME = BASEURL +"smvc/simpleForms/home";
	public static final String ADDRESSFORM = BASEURL + "smvc/simpleForms/addaddressform";
	public static final String EDIT = BASEURL + "smvc/simpleForms/edit";

}