package uk.co.brightfuture.examplespringwebapp.web.lib.selenium;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.*;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.TimeUnit;

import net.anthavio.phanbedder.Phanbedder;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Test base which uses phantom js driver, also has screenshot method for
 * debugging purposes.
 * 
 * @author LRogers
 *
 */
public class SeleniumTestBase {

	/*
	 * Use these constants with driver.manage.window.setsize in the setup
	 * method. To change to specific resolution.
	 */
	private final static Dimension RESOLUTION_768 = new Dimension(1366, 768);
	private final static Dimension RESOLUTION_1080 = new Dimension(1920, 1080);
	private final static Dimension RESOLUTION_900 = new Dimension(1440, 900);

	protected URI baseUrl;
	protected WebDriver driver;

	@Before
	public void setUp() throws Exception {

		// Can be replaced with firefox driver for debugging and making tests.
		// Phantom js is a headless
		// browser so it will not pop up or be visible at all.
//		driver = new PhantomJSDriver(setDesiredCapabilities());
		 driver = new FirefoxDriver(setDesiredCapabilities());

		// ImplicitlyWait will poll the DOM every 500 milliseconds until the
		// element is found (or timeout after 9 seconds)
		driver.manage().timeouts().implicitlyWait(WaitTool.DEFAULT_WAIT_4_PAGE, TimeUnit.SECONDS);

		// Allows file navigator to be started -
		// https://www.browserstack.com/automate/java#enhancements-uploads-downloads
//		((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());

		// Go to home page
		driver.get(PagePath.BASEURL);

		// Change dimension for resolution testing
		driver.manage().window().setSize(RESOLUTION_768);
	}

	/**
	 * Sets variables to be used with browser stack.
	 */
	private DesiredCapabilities setDesiredCapabilities() {
		// This basically sets up phantom js jar on the computer it runs.
		File phantomjs = Phanbedder.unpack();

		DesiredCapabilities capability = new DesiredCapabilities();
		capability
				.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, phantomjs.getAbsolutePath());
		capability.setJavascriptEnabled(true);
		capability.setCapability("takesScreenshot", true);

		return capability;
	}

	/**
	 * For debug purposes only. Will place a screenshot on c:\ drive with
	 * filename given Use this to get visual feedback on your test. If this is
	 * inadequate replace phantomjs browser with firefox browser. This will run
	 * firefox which means you must have it installed.
	 */
	public void takeScreenShotWithPhantomJs(String filename) {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File("C:\\" + filename + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used for confirming current location
	 * 
	 * @param string
	 * @return if driver is on current page or not
	 */
	public void assertIsOnThisPage(String url) {
		assertThat(driver.getCurrentUrl(), is(url));
	}

	public void assertIsOnThisPageContains(String url) {
		assertThat(driver.getCurrentUrl(), containsString(url));
	}

	/**
	 * Used for confirming current location
	 * 
	 * @param string
	 * @return if driver is on current page or not
	 */
	public boolean isOnThisPage(String url) {
		String currentURL = driver.getCurrentUrl();
		System.out.println(currentURL);
		return driver.getCurrentUrl().equals(url);
	}

	/**
	 * Set the driver implicitlyWait time.
	 * 
	 * @param waitTimeInSeconds
	 */
	public void setImplicitlyWaitTime(int waitTimeInSeconds) {
		driver.manage().timeouts().implicitlyWait(waitTimeInSeconds, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
