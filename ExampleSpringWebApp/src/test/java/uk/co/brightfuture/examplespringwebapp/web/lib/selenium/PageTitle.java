package uk.co.brightfuture.examplespringwebapp.web.lib.selenium;

/**
 * Represents the page title in the browser
 * 
 * @author JWatkins
 *
 */
public class PageTitle {
	
	public static final String BASEURL = "Example";
	public static final String HOME = "JSP Example";
	public static final String ADDRESSFORM = "Add Contact";
	public static final String EDIT = "Edit Contact";
}