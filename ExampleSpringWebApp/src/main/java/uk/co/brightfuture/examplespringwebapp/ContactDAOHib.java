/**
 * 
 */
package uk.co.brightfuture.examplespringwebapp;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import uk.co.brightfuture.examplespringwebapp.web.forms1.Contact;

/**
 * Implementation of the person DAO using JDBCTemplate
 * @author lpage
 */
@Repository
@EnableTransactionManagement
@Transactional
public class ContactDAOHib  implements ContactDAO
{
	/* The @Autowired annotation means that sessionFactory will be instantiated only once across the whole program 
	 * by Spring. This is much more efficient that creating a new one for each controller. Session Factories are also 
	 * heavy on processing due to loading the drivers etc.
	 */
	@Autowired @Qualifier("sessionFactory")
	private SessionFactory sessionFactory;
	/**
	 * Returns a person via id
	 * @param Long id
	 * @return Person
	 */
	@Override
	public Contact retrieveContact(int id)
	{
		System.out.println("oyky");
		return (Contact) sessionFactory.getCurrentSession().load(Contact.class, id);
	}
	/**
	 * Retrieves all the people
	 * @return people
	 */
	@Override
	public List<Contact> retrieveAllContacts()
	{
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Contact.class);
		@SuppressWarnings("unchecked")
		List<Contact> contact = criteria.list();
		return contact;
	}
	/**
	 * Search for a person using specific details
	 */
	@Override
	public Contact findByExample(Contact contact)
	{
		Criteria createCriteria = sessionFactory.getCurrentSession().createCriteria(contact.getClass());
		Contact c =  (Contact) createCriteria.add(Example.create(contact)).list().get(0);
		return c;
	}
	/**
	 * Deletes a person from database using their id
	 * @param Long id
	 */
	@Override
	public void deleteContact(int id) {
		Contact contact = (Contact) sessionFactory.getCurrentSession().load(Contact.class,id);
		sessionFactory.getCurrentSession().delete(contact);	
	}
	/**
	 * Creates a new person in the database
	 */
	@Override
	public void createContact(Contact contact){
		Session session=sessionFactory.getCurrentSession();
		session.save(contact);
	}
	
	@Override
	public void update(Contact contact) {
		Session session=sessionFactory.getCurrentSession();
		session.update(contact);
	}
}