package uk.co.brightfuture.examplespringwebapp.web.forms1;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import uk.co.brightfuture.examplespringwebapp.ContactDAO;


@Controller
@RequestMapping("/simpleForms")
public class SimpleFormsController
{
	@Autowired 
	private FormValidator formValidator;
	
	@Autowired
	@Qualifier("contactDAOHib")
	private ContactDAO contactdao;


	
	@RequestMapping("/home")
	public String AddANewAddress()
	{
		
		return "simpleforms/home";
	}
	
	
	@RequestMapping("/addaddressform")
	public String addAddressForm(HttpSession session)
	{
		session.removeAttribute("firstname");
		session.removeAttribute("surname");
		session.removeAttribute("phone");
		session.removeAttribute("workno");
		session.removeAttribute("homeno");
		session.removeAttribute("address");
		session.removeAttribute("email");
		
		return "simpleforms/addContact";
	}
	
	@RequestMapping("/formtomodel")
	public  ModelAndView formToModel(@ModelAttribute Contact contact, BindingResult binding, HttpSession session)
	{
		ModelAndView modelAndView = new ModelAndView("redirect:/smvc/simpleForms/home");
		formValidator.validate(contact, binding);
		if(binding.hasErrors())
		{
			for (FieldError error: binding.getFieldErrors("name"))
			{	
				modelAndView.addObject("nameerror",error.getDefaultMessage());
			}
			for (FieldError error: binding.getFieldErrors("surname"))
			{
				modelAndView.addObject("surnameerror",error.getDefaultMessage());
			}
			for (FieldError error: binding.getFieldErrors("phone"))
			{
				modelAndView.addObject("phoneerror",error.getDefaultMessage());
			}
			for (FieldError error: binding.getFieldErrors("workno"))
			{
				modelAndView.addObject("worknoerror",error.getDefaultMessage());
			}
			for (FieldError error: binding.getFieldErrors("homeno"))
			{
				modelAndView.addObject("homenoerror",error.getDefaultMessage());
			}
			for (FieldError error: binding.getFieldErrors("Email"))
			{
				modelAndView.addObject("Emailerror",error.getDefaultMessage());
			}
			
			session.setAttribute("id" , contact.getId());
			session.setAttribute("firstname" , contact.getName());
			session.setAttribute("surname" , contact.getSurname());
			session.setAttribute("phone" , contact.getPhone());
			session.setAttribute("workno" , contact.getWorkno());
			session.setAttribute("homeno" , contact.getHomeno());
			session.setAttribute("address" , contact.getAddress());
			session.setAttribute("email" , contact.getEmail());
			modelAndView.addObject("session",session);	
			modelAndView.setViewName("simpleforms/addContact");
			
		}
		else
		{
			
			contactdao.createContact(contact);
			modelAndView.addObject("contactList", contactdao.retrieveAllContacts());
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/home", method = RequestMethod.GET)
	public ModelAndView getData()
	{
		ModelAndView model = new ModelAndView("simpleforms/home");
		model.addObject("contactList", contactdao.retrieveAllContacts());
		
		return model;
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView editContact(@PathVariable("id") int id)
	{
		ModelAndView modelAndView = new ModelAndView("simpleforms/edit");
		modelAndView.addObject("contact", contactdao.retrieveContact(id));
		return modelAndView;
	}
	
	@RequestMapping(value = "/delete/{id}")
    public String removeContact(@PathVariable("id") int id)
	{
        this.contactdao.deleteContact(id);
        return "redirect:/smvc/simpleForms/home";
    }
	
	
	@RequestMapping(value = "/edit/update")
	public ModelAndView updateContact(@ModelAttribute Contact contact, BindingResult binding)
	{
		ModelAndView modelAndView = new ModelAndView("redirect:/smvc/simpleForms/home");
		
		formValidator.validate(contact, binding);
		if(binding.hasErrors())
		{
			for (FieldError error: binding.getFieldErrors("name"))
			{	
				modelAndView.addObject("nameerror",error.getDefaultMessage());
			}
			for (FieldError error: binding.getFieldErrors("surname"))
			{
				modelAndView.addObject("surnameerror",error.getDefaultMessage());
			}
			for (FieldError error: binding.getFieldErrors("phone"))
			{
				modelAndView.addObject("phoneerror",error.getDefaultMessage());
			}
			for (FieldError error: binding.getFieldErrors("workno"))
			{
				modelAndView.addObject("worknoerror",error.getDefaultMessage());
			}
			for (FieldError error: binding.getFieldErrors("homeno"))
			{
				modelAndView.addObject("homenoerror",error.getDefaultMessage());
			}
			for (FieldError error: binding.getFieldErrors("Email"))
			{
				modelAndView.addObject("Emailerror",error.getDefaultMessage());
			}
				modelAndView.setViewName("simpleforms/edit");
		}
		else
		{	
			this.contactdao.update(contact);
			modelAndView.addObject("contactList", contactdao.retrieveAllContacts());
		}
		
		return modelAndView;
	}
	
}
