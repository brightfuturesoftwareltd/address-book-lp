/**
 * 
 */
package uk.co.brightfuture.examplespringwebapp.web.forms1;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component("theModelValidator")
public class FormValidator implements Validator
{

	@Override
	public boolean supports(Class<?> clazz)
	{
		return Contact.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		
		Contact contact = (Contact)target;
		
		
		
		
		if(contact.getName().matches("[a-zA-Z]+"));
		else
			errors.rejectValue("name", "XXX", "Please only enter letters into the 'First Name' field");
		if(contact.getSurname().matches("[a-zA-Z]+"));
		else
			errors.rejectValue("surname", "abc", "Please only enter letters into the 'Last Name' field");
		if(contact.getPhone().length() != 11)
			errors.rejectValue("phone", "XXX", "The phone number must be 11 digits");
		if(contact.getWorkno().length() != 11)
			errors.rejectValue("workno", "XXX", "The work number must be 11 digits");
		if(contact.getHomeno().length() != 11)
			errors.rejectValue("homeno", "XXX", "The home number must be 11 digits");
		if(contact.getAddress().contains(";"))
			errors.rejectValue("address", "XXX", "Please enter a valid address");
		if(contact.getEmail().contains("."));
		else
			errors.rejectValue("Email", "XXX", "Please enter a valid email address");
		if(contact.getPhone().matches("[0-9]+"));
		else
			errors.rejectValue("phone", "XXX", "Please only enter numbers into the 'Phone Number' field");
			
		if(contact.getWorkno().matches("[0-9]+"));
		else
			errors.rejectValue("workno", "XXX", "Please only enter numbers into the 'work Number' field");
		
		if(contact.getHomeno().matches("[0-9]+"));
		else
			errors.rejectValue("homeno", "XXX", "Please only enter numbers into the 'home Number' field");
			
	}
}