/**
 * 
 */
package uk.co.brightfuture.examplespringwebapp;

import java.util.List;

import uk.co.brightfuture.examplespringwebapp.web.forms1.Contact;


/**
 * 
 * @author lpage
 */
public interface ContactDAO
{
	/**
	 * Retrieve the contact details for the given ID
	 * @param id The primary key of the contact
	 * @return The contact details
	 */
	public Contact retrieveContact(int id);
	
	public List<Contact> retrieveAllContacts();
	
	/**
	 * Creates the contact in the database
	 * @param contact
	 */
	public void createContact(Contact contact);
	
	/**
	 * Deletes a contact using the id of a contact if they are no longer needed
	 * @param id
	 */
	public void deleteContact(int id);
	
	/**
	 * find any contact in the database
	 * @param contact
	 * @return contact
	 */
	
	public Contact findByExample(Contact contact);
	
	
	public void update(Contact contact);
}