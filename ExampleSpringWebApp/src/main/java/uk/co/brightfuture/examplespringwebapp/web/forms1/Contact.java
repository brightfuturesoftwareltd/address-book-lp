/**
 * 
 */
package uk.co.brightfuture.examplespringwebapp.web.forms1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;



@Entity
@Table(name="contacts")
@Proxy(lazy=false)
public class Contact
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name = "first_name")
	private String name;
	@Column(name = "last_name")
	private String surname;
	@Column(name = "phone_number")
	private String phone;
	@Column(name = "work_number")
	private String workno;
	@Column(name = "home_number")
	private String homeno;
	@Column(name = "address")
	private String address;
	@Column(name = "email")
	private String email;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}
	
	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	
	public String getWorkno()
	{
		return workno;
	}

	public void setWorkno(String workno)
	{
		this.workno = workno;
	}
	
	public String getHomeno()
	{
		return homeno;
	}

	public void setHomeno(String homeno)
	{
		this.homeno = homeno;
	}
	
	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}
	
	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}
}
