<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>Add Contact</title>
    </head>
    <style>
    	  body {
	background: #fafafa url(http://jackrugile.com/images/misc/noise-diagonal.png);
	color: #444;
	font: 100%/30px 'Helvetica Neue', helvetica, arial, sans-serif;
	text-shadow: 0 1px 0 #fff;
}

strong {
	font-weight: bold; 
}

em {
	font-style: italic; 
}

table {
	background: #f5f5f5;
	border-collapse: separate;
	box-shadow: inset 0 1px 0 #fff;
	font-size: 12px;
	line-height: 24px;
	margin: 30px auto;
	text-align: left;
	width: 800px;
}	

th {
	background: url(http://jackrugile.com/images/misc/noise-diagonal.png), linear-gradient(#777, #444);
	border-left: 1px solid #555;
	border-right: 1px solid #777;
	border-top: 1px solid #555;
	border-bottom: 1px solid #333;
	box-shadow: inset 0 1px 0 #999;
	color: #fff;
  font-weight: bold;
	padding: 10px 15px;
	position: relative;
	text-shadow: 0 1px 0 #000;	
}

th:after {
	background: linear-gradient(rgba(255,255,255,0), rgba(255,255,255,.08));
	content: '';
	display: block;
	height: 25%;
	left: 0;
	margin: 1px 0 0 0;
	position: absolute;
	top: 25%;
	width: 100%;
}

th:first-child {
	border-left: 1px solid #777;	
	box-shadow: inset 1px 1px 0 #999;
}

th:last-child {
	box-shadow: inset -1px 1px 0 #999;
}

td {
	border-right: 1px solid #fff;
	border-left: 1px solid #e8e8e8;
	border-top: 1px solid #fff;
	border-bottom: 1px solid #e8e8e8;
	padding: 10px 15px;
	position: relative;
	transition: all 300ms;
}

td:first-child {
	box-shadow: inset 1px 0 0 #fff;
}	

td:last-child {
	border-right: 1px solid #e8e8e8;
	box-shadow: inset -1px 0 0 #fff;
}	

tr {
	background: url(http://jackrugile.com/images/misc/noise-diagonal.png);	
}

tr:nth-child(odd) td {
	background: #f1f1f1 url(http://jackrugile.com/images/misc/noise-diagonal.png);	
}

tr:last-of-type td {
	box-shadow: inset 0 -1px 0 #fff; 
}

tr:last-of-type td:first-child {
	box-shadow: inset 1px -1px 0 #fff;
}	

tr:last-of-type td:last-child {
	box-shadow: inset -1px -1px 0 #fff;
}	

.btn {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
}

.btn:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}    
    </style>
    <body>			
   		<a class="btn" href="\ExampleSpringWebApp/smvc/simpleForms/home" > Home</a>
    
        <form method="post" action="formtomodel">
            <table border="1" >
                <thead>
                    <tr>
                        <th colspan="2">Add Contact</th>
                    </tr>
                </thead>
                <tbody>
                
                    <tr>
                        <td>First Name</td>
                        <td>
	                        <input type="text" name="name" value="${firstname}" required title="First Name must only include letters"/>
		                      	<c:if test="${!empty nameerror }" >
									<img src="http://png-1.findicons.com/files/icons/1686/led/16/cross.png" />
							   		${nameerror }
						    	</c:if>
                        </td>
                     	    
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td>
                       	 	<input type="text" name="surname" value="${surname }" required title="last Name must only include letters" />
                        	<c:if test="${!empty surnameerror }" >
								<img src="http://png-1.findicons.com/files/icons/1686/led/16/cross.png" />
								${surnameerror }
    						</c:if>
                        </td> 
                            
                    </tr>
                    <tr>
                        <td>Phone number</td>
                        <td>
	                        <input type="text" name="phone" value="${phone }" required title="Phone Number must be 11 digits long"/>
		                    <c:if test="${!empty phoneerror }" >
								<img src="http://png-1.findicons.com/files/icons/1686/led/16/cross.png" />
								${phoneerror }
	    					</c:if>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Work Number</td>
                        <td>
	                        <input type="text" name="workno" value="${workno }" required title="work number must be 11 digits long" />
	                        <c:if test="${!empty worknoerror }" >
								<img src="http://png-1.findicons.com/files/icons/1686/led/16/cross.png" />
								${worknoerror }
	    					</c:if>
                        </td>
                    </tr>
                    
                	<tr>
                        <td>Home Number</td>
                        <td>
	                        <input type="text" name="homeno" value="${homeno }" required title="Home number must be 11 digits long"/>
	                        <c:if test="${!empty homenoerror }" >
								<img src="http://png-1.findicons.com/files/icons/1686/led/16/cross.png" />
								${homenoerror }
	    					</c:if>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Address</td>
                        <td>
                       		<input type="text" name="Address" value="${address }" required title="address must be only include letters"/> 
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Email Address</td>
                        <td>
	                        <input type="email" name="Email" value="${email }" required/>
	                         <c:if test="${!empty Emailerror }" >
								<img src="http://png-1.findicons.com/files/icons/1686/led/16/cross.png" />
								${Emailerror }
	    					</c:if>
                        </td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td><input class="btn" type="submit" value="Add Contact" /></td>
                    </tr>
                </tbody>
            </table>
        </form>
	</body>
</html>